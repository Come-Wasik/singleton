<?php

namespace Nolikein\Singleton\Testing;

use Nolikein\Singleton\SingletonTrait;


class TraitedTwo
{
    use SingletonTrait;

    /** @var string Name */
    public $name = '';
}
