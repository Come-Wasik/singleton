<?php

namespace Nolikein\Singleton\Testing;

use Nolikein\Singleton\SingletonClass;

class ParentClass extends SingletonClass
{
    /** @var string Name */
    public $name = '';
}
