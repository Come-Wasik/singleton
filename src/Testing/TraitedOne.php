<?php

namespace Nolikein\Singleton\Testing;

use Nolikein\Singleton\SingletonTrait;


class TraitedOne
{
    use SingletonTrait;

    /** @var string Name */
    public $name = '';
}
