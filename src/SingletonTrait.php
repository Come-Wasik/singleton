<?php

namespace Nolikein\Singleton;

trait SingletonTrait
{
    /** @var array<class-string, static> The self object registered */
    private static array $selves = [];

    /**
     * Retrieve the curent instance of the class.
     *
     * @return static
     */
    public static function getInstance(): mixed
    {
        /** @phpstan-ignore staticClassAccess.privateProperty */
        if ( ! array_key_exists(static::class, static::$selves)) {
            /** @phpstan-ignore staticClassAccess.privateProperty,new.static */
            static::$selves[static::class] = new static();
        }

        /** @phpstan-ignore staticClassAccess.privateProperty */
        return static::$selves[static::class];
    }

    /**
     * Prevent the creation of a new object.
     */
    public function __construct()
    {
    }

    /**
     * Prevent the copy of the current object.
     *
     * @codeCoverageIgnore
     */
    public function __clone(): void
    {
    }

    /**
     * Prevent an unserialisation of the current object.
     *
     * @codeCoverageIgnore
     */
    public function __wakeup(): void
    {
    }
}
