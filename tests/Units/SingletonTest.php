<?php

declare(strict_types=1);

namespace Tests\Units;

use Nolikein\Singleton\Testing\ChildClass;
use Nolikein\Singleton\Testing\ParentClass;
use Nolikein\Singleton\Testing\TraitedOne;
use Nolikein\Singleton\Testing\TraitedTwo;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Nolikein\Singleton\Testing\ParentClass::__construct
 * @covers \Nolikein\Singleton\Testing\ParentClass::getInstance
 * @covers \Nolikein\Singleton\Testing\ChildClass::__construct
 * @covers \Nolikein\Singleton\Testing\ChildClass::getInstance
 * @covers \Nolikein\Singleton\Testing\TraitedOne::__construct
 * @covers \Nolikein\Singleton\Testing\TraitedOne::getInstance
 * @covers \Nolikein\Singleton\Testing\TraitedTwo::__construct
 * @covers \Nolikein\Singleton\Testing\TraitedTwo::getInstance
 */
class SingletonTest extends TestCase
{
    /**
     * @test
     */
    public function canUseParentOnly(): void
    {
        $instance = ParentClass::getInstance();
        $name = $instance->name = 'Li';

        $this->assertEquals($name, ParentClass::getInstance()->name);
    }

    /**
     * @test
     */
    public function canUseParentAndChild(): void
    {
        $parent = ParentClass::getInstance();
        $parentName = $parent->name = 'Li';
        $child = ChildClass::getInstance();
        $childName = $child->name = 'La';

        $this->assertEquals($parentName, actual: ParentClass::getInstance()->name);
        $this->assertEquals($childName, ChildClass::getInstance()->name);
    }


    /**
     * @test
     */
    public function canUseOneTrait(): void
    {
        $instance = TraitedOne::getInstance();
        $name = $instance->name = 'Li';

        $this->assertEquals($name, TraitedOne::getInstance()->name);
    }

    /**
     * @test
     */
    public function canUseTwoTraits(): void
    {
        $instance = TraitedOne::getInstance();
        $nameOne = $instance->name = 'Li';
        $instance = TraitedTwo::getInstance();
        $nameTwo = $instance->name = 'Li';

        $this->assertEquals($nameOne, TraitedOne::getInstance()->name);
        $this->assertEquals($nameTwo, TraitedTwo::getInstance()->name);
    }
}
