# Singleton

![PHP](https://img.shields.io/packagist/php-v/nolikein/singleton) ![Packagist version](https://img.shields.io/packagist/v/nolikein/singleton) ![Packagist License](https://img.shields.io/packagist/l/nolikein/singleton) ![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline/Come-Wasik/singleton/master) ![Gitlab Code Coverage](https://img.shields.io/gitlab/pipeline-coverage/Come-Wasik%2Fsingleton?job_name=test-php&branch=master)

A librairy to use the [singleton](https://designpatternsphp.readthedocs.io/en/latest/Creational/Singleton/README.html) design pattern. Thanks to the project of [domnikl](https://github.com/domnikl/DesignPatternsPHP/tree/main/Creational/Singleton).

## Installation

Install and Use [Composer](https://getcomposer.org/):

### PHP 8.3

    composer require nolikein/singleton ^2.0.0

### PHP 8.2 and bellow

    composer require nolikein/singleton ^1.1.0

## Usage

You can create a singleton from 2 ways.

### Extending

```php
use Nolikein\Singleton\SingletonClass;

class MyClass extends SingletonClass {}
```

### Using trait

```php
use Nolikein\Singleton\SingletonTrait;

class MyClass
{
    use SingletonTrait;
}
```
Finally, you can create an instance:
```php
$instance = MyClass::getInstance();
```

## Testing

### Run tests with coverage

**displayed as html in tests/Coverage directory**

    docker compose run --rm php ./vendor/bin/phpunit --configuration phpunit.xml --coverage-text --color
    sudo chown $USER:$USER -R tests/Coverage

## Licence

The project is under lincence [MIT](https://opensource.org/licenses/MIT).
